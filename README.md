# aws-extra-one

Layer one, the lowest layer of aws-extra. Full documentation is available [here](https://gitlab.com/shaungreen/aws-extra-one/wikis/aws-extra-one).

## What do you get?
* promisfied aws-sdk (all of it)
* automatic, configurable logging of sdk calls
* automatic pagination of paged sdk calls like `lambda.listFunctions`

## What does it cost?
Layer one currently weighs in at 164 lines of code (a whopping 11Kb).  It brings in no other dependencies.  The aws-sdk is listed as a peer dependency because if you are deploying to AWS Lambda, there is already a copy available on the node path anyway.  Also, this allows you to inject your own preferred version of aws-sdk.  

The code makes use of async/await to reduce code size, so you will require node8+.
